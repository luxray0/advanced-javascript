
/// 1. Конструкцію try...catch можна використовувати при роботі з файлами, при зчитуванні або записі файлів можуть виникати помилки, такі як відсутність файлу або проблеми з правами доступу.
/// 2. Робота з мережевими запитами - при взаємодії з сервером можуть виникати помилки, такі як відсутність з'єднання або невідповідь сервера.
/// 3. Робота з базою даних - при зверненні до бази даних можуть виникати помилки, такі як відмова у з'єднанні або проблеми з запитами.

/// Конструкція try...catch дозволяє обробляти такі помилки та повідомляти користувача про проблему.

//////////////////////////////////////////////////////////////////
/// Дано масив books.
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70,
        year: 2019
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];
/// Функція яка створює та повертає, а також додає на сторінку div з id 'root'. 
function newElement(element, id, child) {
    const newDiv = document.createElement(element);
    newDiv.id = id;
    document.body.appendChild(newDiv);
    newDiv.appendChild(child)
    return newDiv;
}
/// Функція-замикання яка повертає функцію яка в свою чергу перевіряє потрібні властивості.
function createIsValidBook(property) {
    return function isValidBook(book) {
        try {
            property.forEach(prop => {
                if (!book.hasOwnProperty(prop)) {
                    throw prop;
                }
            });
            return true;
        } catch (error) {
            console.error(`Помилка: У книжці "${book.name}" відсутня властивість: "${error}"`);
            return false;
        }
    }
}
/// Функція яка створює та повертає елемент ul в який додає елементи li які пройшли валідацію за порібними властивостями.
function renderBookList(books) {
    const ul = document.createElement('ul');
    books.forEach(book => {
        if (isValidBook(book)) {
            const li = createBookListItem(book);
            ul.appendChild(li);
        }
    });
    return ul;
}
/// Функція яка створює та повертає елемент li з властивостями які виводимо на сторінку.
function createBookListItem(book) {
    const li = document.createElement('li');
    const bookDetails = `Автор: ${book.author}, Назва: "${book.name}", Ціна: ${book.price} грн.`;
    li.textContent = bookDetails;
    return li;
}
const isValidBook = createIsValidBook(['author', 'name', 'price']);
const bookList = renderBookList(books);
newElement('div', 'root', bookList);