const findIpButton = document.querySelector('#find-id')
const hideIpButton = document.querySelector('#hide-id-btn')
const loaderAnimation = document.querySelector('.loader')

findIpButton.addEventListener('click', (e) => {
    e.preventDefault();
    loaderAnimation.style.display = 'block';

    const findId = async () => {
        const getIpURL = 'https://api.ipify.org/?format=json'
        try {
            const response = await fetch(getIpURL);
            const data = await response.json();
            const ipAddress = data.ip;
            const result = await getAddress(ipAddress)
            if (result) {
                const [continent, country, region, city, district] = result;

                const ipCard = document.querySelector('.btn-card');

                const ipInfo = document.createElement('div');
                ipInfo.className = 'ip-info'

                const ip = document.createElement('h1');
                ip.innerHTML = `<span>IP адреса:</span> ${ipAddress}`;
                const ipContinent = document.createElement('h2');
                ipContinent.innerHTML = `<span>Континент:</span> ${continent}`;
                const ipCountry = document.createElement('h3');
                ipCountry.innerHTML = `<span>Країна:</span> ${country}`;
                const ipRegion = document.createElement('h4');
                ipRegion.innerHTML = `<span>Регіон:</span> ${region}`;
                const ipCity = document.createElement('h5');
                ipCity.innerHTML = `<span>Місто:</span> ${city}`;

                ipInfo.appendChild(ip);
                ipInfo.appendChild(ipContinent);
                ipInfo.appendChild(ipCountry);
                ipInfo.appendChild(ipRegion);
                ipInfo.appendChild(ipCity);

                ipCard.insertAdjacentElement('afterbegin', ipInfo);
            }
        } catch (err) {
            console.error('Error >>', err);
        }
        findIpButton.style.display = 'none'
        hideIpButton.style.display = 'block'
        loaderAnimation.style.display = 'none';
    }


    const getAddress = async (ipad) => {
        const ipAddressURL = `http://ip-api.com/json/${ipad}?fields=1110015`
        try {
            const response = await fetch(ipAddressURL)
            const data = await response.json()
            if (data.status === "success") {
                console.log(data)
                return [data.continent, data.country, data.regionName, data.city, data.district];
            }
        } catch (err) {
            console.error('Error >>', err)
        }
    }

    findId()

})

hideIpButton.addEventListener('click', (e) => {
    e.preventDefault()

    document.querySelector('.ip-info').remove()
    findIpButton.style.display = 'block'
    hideIpButton.style.display = 'none'
})