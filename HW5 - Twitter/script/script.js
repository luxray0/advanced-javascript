const usersURL = "https://ajax.test-danit.com/api/json/users"
const postsURL = "https://ajax.test-danit.com/api/json/posts"
const photoURL = "https://ajax.test-danit.com/api/json/photos"

class Card {
    constructor(user, post, photo) {
        this.user = user;
        this.post = post;
        this.photo = photo;
        this.deleteButton = document.createElement("div");
        this.deleteButton.classList.add('delete-btn')
        this.deleteButton.addEventListener("click", () => this.deleteCard());
    }

    createCard() {

        const cardList = document.createElement("div");
        cardList.className = "card-list"

        const card = document.createElement("div");
        card.className = "card";

        const textItems = document.createElement("div");
        textItems.className = "text-items";

        const name = document.createElement("h4");
        name.innerHTML = `${this.user.name} <a href = "mailto: ${this.user.email}">@${this.user.email} · 3h</a>`;

        const postTitle = document.createElement("h6");
        postTitle.innerHTML = this.post.title;

        const postGraph = document.createElement("h5");
        postGraph.innerHTML = this.post.body;

        const userPhoto = document.createElement('img')
        userPhoto.src = this.photo.url

        card.appendChild(userPhoto);
        textItems.appendChild(name);
        textItems.appendChild(postTitle);
        textItems.appendChild(postGraph);

        card.appendChild(textItems)
        card.appendChild(this.deleteButton)
        cardList.appendChild(card);

        document.body.appendChild(cardList)
    }
    deleteCard() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
            method: "DELETE"
        })
            .then(response => {
                if (response.ok) {
                    this.deleteButton.parentElement.remove();
                }
            })
            .catch(error => console.error("Запит на видалення не виконано.", error))
    };
}

const getUsers = () => {
    return fetch(usersURL)
        .then(response => response.json())
        .then(data => {
            users = data;
            return data;
        })
        .catch(error => console.error("Error!>>", error))
};

const getPosts = () => {
    return fetch(postsURL)
        .then(response => response.json())
        .then(data => data.slice(0, 10))
        .catch(error => console.error("Error!>>", error))
};
const getPhotos = () => {
    return fetch(photoURL)
        .then(response => response.json())
        .then(data => {
            photos = data;
            return data;
        })
        .catch(error => console.error("Error!>>", error))
};



Promise.all([getUsers(), getPosts(), getPhotos()])
    .then(([users, posts, photos]) => {
        setTimeout(() => {
            users.forEach(user => {
                const post = posts.find(post => post.id === user.id);
                const photo = photos.find(photo => photo.id === user.id);
                const card = new Card(user, post, photo);
                card.createCard();

                loadingElement.remove()
                twitterIcon.style.display = "block"
            });
        }, 400)


    })
    .catch(err => console.error("Error! >>", err));

///-------------------------------------------------------\\\

const loadingElement = document.createElement('div');
loadingElement.classList.add('lds-dual-ring');
document.body.appendChild(loadingElement);

///-------------------------------------------------------\\\

const twitterIcon = document.createElement("img");
twitterIcon.classList.add('twit-icon')
twitterIcon.style.display = "none"
twitterIcon.src = "https://www.freepnglogos.com/uploads/twitter-logo-png/twitter-logo-vector-png-clipart-1.png"
document.body.appendChild(twitterIcon)