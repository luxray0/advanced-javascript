const starWarsURL = "https://ajax.test-danit.com/api/swapi/films"

const getMovies = (url) => {
  fetch(url)
    .then(response => response.json())
    .then(movies => {
      renderMovies(movies)
    })
    .catch(error => console.error(error))
}

const renderMovies = (movies) => {
  const movieList = document.createElement('div');
  movieList.classList.add('movie-card')
  movies.forEach((movie) => {
    const movieItem = document.createElement('li');
    movieItem.classList.add("movie-list")

    const { episodeId, name, openingCrawl } = movie;

    const episodeName = document.createElement('h2');
    episodeName.textContent = `Episode ${episodeId}: ${name}`;

    const crawl = document.createElement('p');
    crawl.textContent = openingCrawl;

    movieItem.appendChild(episodeName)
    movieItem.appendChild(crawl)

    movieList.appendChild(movieItem)

    document.body.appendChild(movieList)

    getCharacters(movie.characters, movieItem);
  });
}

const getCharacters = (charactersURL, movieItem) => {
  const loadingElement = document.createElement('div');
  loadingElement.classList.add('lds-dual-ring');
  movieItem.appendChild(loadingElement);

  Promise.all(charactersURL.map(url => fetch(url).then(response => response.json())))
    .then(characters => {
      const characterList = document.createElement('ul');
      characterList.classList.add('character-list')
      characterList.innerHTML = '<strong>Characters:</strong>';

      characters.forEach(character => {
        const characterItem = document.createElement('li');
        characterItem.classList.add('character-item')
        characterItem.textContent = character.name;
        characterList.appendChild(characterItem)
      });

      movieItem.appendChild(characterList)
      loadingElement.classList.remove('lds-dual-ring')
    })
    .catch(error => console.error(error))


}
getMovies(starWarsURL)