// 1. 
/// Коли ми викликаємо метод чи властивість об'єкту, JavaScript спочатку шукає їх серед властивостей та методів цього об'єкту. Якщо властивість або метод не знайдено, JavaScript звертається до прототипу цього об'єкту та шукає властивість або метод там. Якщо властивість або метод знайдено в прототипі, вони використовуються для поточного об'єкту. Цей процес продовжується вглиб за потребою, поки не буде знайдено властивість або метод або ж не буде досягнуто самого кінця ланцюга прототипів.
// 2.
/// Це дозволяє класу-нащадку додати свої властивості та методи на основі батьківського класу. Це потрібно, коли клас-нащадок має додаткові властивості та методи порівняно з батьківським класом, але він все ще повинен мати ті ж базові властивості та методи.


class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  get age() {
    return this._age;
  }

  get salary() {
    return this._salary;
  }

  set name(value) {
    this.name = value;
  }

  set age(value) {
    this.age = value;
  }

  set salary(value) {
    this.salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }
}

const programmer1 = new Programmer('Yan', 26, 3000, ['JavaScript']);
const programmer2 = new Programmer('Daria', 25, 900, ['Java', 'Pyton']);
const programmer3 = new Programmer('Alex', 29, 1200, ['Java', 'C++']);

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);
